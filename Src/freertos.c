/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "main.h"
#include "Tube_Ind.h"
#include "LCD_I2C.h"
#include "DS3231.h"
#include "dht22.h"
#include "stdlib.h"
#include "iwdg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern FlagTimeSet flag;
uint8_t flag_TypeOut = 1;
uint8_t flag_TypeOutOld = 0;
/* USER CODE END Variables */
osThreadId Tube_Ind_TaskHandle;
osThreadId DHT22_TaskHandle;
osThreadId LCD_TaskHandle;
osThreadId DS3231_TaskHandle;
osTimerId ChOutDataTimerHandle;
osSemaphoreId DS3231_TimeUpdate_SemHandle;
osSemaphoreId LCD_TimeFromDSHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void Start_Tube_Ind_Task(void const * argument);
void Start_DHT22_Task(void const * argument);
void Start_LCD_Task(void const * argument);
void Start_DS3231_Task(void const * argument);
void ChOutDataTimerCall(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	flag = NoSet;
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of DS3231_TimeUpdate_Sem */
  osSemaphoreDef(DS3231_TimeUpdate_Sem);
  DS3231_TimeUpdate_SemHandle = osSemaphoreCreate(osSemaphore(DS3231_TimeUpdate_Sem), 1);

  /* definition and creation of LCD_TimeFromDS */
  osSemaphoreDef(LCD_TimeFromDS);
  LCD_TimeFromDSHandle = osSemaphoreCreate(osSemaphore(LCD_TimeFromDS), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of ChOutDataTimer */
  osTimerDef(ChOutDataTimer, ChOutDataTimerCall);
  ChOutDataTimerHandle = osTimerCreate(osTimer(ChOutDataTimer), osTimerOnce, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of Tube_Ind_Task */
  osThreadDef(Tube_Ind_Task, Start_Tube_Ind_Task, osPriorityRealtime, 0, 128);
  Tube_Ind_TaskHandle = osThreadCreate(osThread(Tube_Ind_Task), NULL);

  /* definition and creation of DHT22_Task */
  osThreadDef(DHT22_Task, Start_DHT22_Task, osPriorityHigh, 0, 128);
  DHT22_TaskHandle = osThreadCreate(osThread(DHT22_Task), NULL);

  /* definition and creation of LCD_Task */
  osThreadDef(LCD_Task, Start_LCD_Task, osPriorityNormal, 0, 256);
  LCD_TaskHandle = osThreadCreate(osThread(LCD_Task), NULL);

  /* definition and creation of DS3231_Task */
  osThreadDef(DS3231_Task, Start_DS3231_Task, osPriorityAboveNormal, 0, 256);
  DS3231_TaskHandle = osThreadCreate(osThread(DS3231_Task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_Start_Tube_Ind_Task */
/**
  * @brief  Function implementing the Tube_Ind_Task thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_Start_Tube_Ind_Task */
void Start_Tube_Ind_Task(void const * argument)
{

  /* USER CODE BEGIN Start_Tube_Ind_Task */
	Tube_Init();
	MX_IWDG_Init();
	/* Infinite loop */
	for(;;)
	{
		Tube_Conv_Time(DS3231_DateTime.Hour, DS3231_DateTime.Minutes);

		Tube_Set_Num_1(Tube_Num_1);
		osDelay(1);

		Tube_Set_Num_2(Tube_Num_2);
		osDelay(1);

		Tube_Set_Num_3(Tube_Num_3);
		osDelay(1);

		Tube_Set_Num_4(Tube_Num_4);
		osDelay(1);
		LL_IWDG_ReloadCounter(IWDG);
	}
  /* USER CODE END Start_Tube_Ind_Task */
}

/* USER CODE BEGIN Header_Start_DHT22_Task */
/**
* @brief Function implementing the DHT22_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_DHT22_Task */
void Start_DHT22_Task(void const * argument)
{
  /* USER CODE BEGIN Start_DHT22_Task */
	uint8_t flag_tmp = 0;
	osDelay(1000);
	dht22_init();
	/* Infinite loop */
	for(;;)
	{
		osDelay(1);
		dht22_GetDataWithRTOS_1();
		osDelay(100);
		dht22_GetDataWithRTOS_2();
		osDelay(18);
		flag_tmp = dht22_GetDataWithRTOS_3();
		if (!flag_tmp) // Если не правильно считана температура
		{
			dht22_init();
			continue;
		}
		osDelay(5);
		temp = dht22_GetTemp();
		osDelay(1);
		hum = dht22_GetHum();
		osDelay(2500);
	}
  /* USER CODE END Start_DHT22_Task */
}

/* USER CODE BEGIN Header_Start_LCD_Task */
/**
* @brief Function implementing the LCD_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_LCD_Task */
void Start_LCD_Task(void const * argument)
{
  /* USER CODE BEGIN Start_LCD_Task */
	char str[16] = {0};

	osDelay(500);
	LCD_I2C_Init();
	osDelay(100);
	LCD_Clear();

	/* Infinite loop */
	for(;;)
	{
		osDelay(1);
		switch (flag)
		{
		case NoSet:
			osSemaphoreWait(LCD_TimeFromDSHandle, 2000);

			if (flag_TypeOut != flag_TypeOutOld)
			{
				if (flag_TypeOut) osTimerStart(ChOutDataTimerHandle, 15000);
				else osTimerStart(ChOutDataTimerHandle, 4000);

				flag_TypeOutOld = flag_TypeOut;
			}

			if (flag_TypeOut) LCD_OutTime(str, &DS3231_DateTime);
			else LCD_OutDate(str, &DS3231_DateTime);

			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Temp %2.1f, %2.1f%%", temp, hum);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}
			break;
		case SetHour:
			sprintf(str, "Setting Mode!!!");
			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Set Hour: %2d    ", DS3231_DateTime.Hour);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			osDelay(70);
			break;
		case SetMin:
			sprintf(str, "Setting Mode!!!");
			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Set Min: %2d    ", DS3231_DateTime.Minutes);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			osDelay(70);
			break;
		case SetYear:
			sprintf(str, "Setting Mode!!!");
			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Set Year: 20%2d  ", DS3231_DateTime.Year);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			osDelay(70);
			break;
		case SetMonth:
			sprintf(str, "Setting Mode!!!");
			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Set Month: %2d   ", DS3231_DateTime.Month);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			osDelay(70);
			break;
		case SetDate:
			sprintf(str, "Setting Mode!!!");
			if (!LCD_SetPos(0, 0))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			sprintf(str, "Set Date: %2d  ", DS3231_DateTime.Date);
			if (!LCD_SetPos(0, 1))
			{
				LCD_I2C_Init();
				continue;
			}
			if (!LCD_Send_Str(str))
			{
				LCD_I2C_Init();
				continue;
			}

			osDelay(70);
			break;
		default:
			break;
		}

	}
  /* USER CODE END Start_LCD_Task */
}

/* USER CODE BEGIN Header_Start_DS3231_Task */
/**
* @brief Function implementing the DS3231_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_DS3231_Task */
void Start_DS3231_Task(void const * argument)
{
  /* USER CODE BEGIN Start_DS3231_Task */
	DS3231_Init();
	char old_time = 0;
	/* Infinite loop */
	for(;;)
	{
		if (flag == NoSet)
		{
			osSemaphoreWait(DS3231_TimeUpdate_SemHandle, 1000);

			DS3231_GetTime(&DS3231_DateTime);
			if (DS3231_DateTime.Minutes != old_time)
			{
				for (uint8_t t = 0; t < 10; t++)
				{
					DS3231_DateTime.Hour = rand() % 24;
					DS3231_DateTime.Minutes = rand() % 60;
					osDelay(100);
				}
				DS3231_GetTime(&DS3231_DateTime);
				old_time = DS3231_DateTime.Minutes;
			}

			osSemaphoreRelease(LCD_TimeFromDSHandle);
		}
		osDelay(1);
	}
  /* USER CODE END Start_DS3231_Task */
}

/* ChOutDataTimerCall function */
void ChOutDataTimerCall(void const * argument)
{
  /* USER CODE BEGIN ChOutDataTimerCall */
	if (flag_TypeOut)
	{
		flag_TypeOut = 0;
	}
	else
	{
		flag_TypeOut = 1;
	}
  /* USER CODE END ChOutDataTimerCall */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
