/*
 * Tube_Ind.c
 *
 *  Created on: 21 апр. 2018 г.
 *      Author: sergey
 */


#include "Tube_Ind.h"

void Tube_Init(void)
{
	Tube_Num_1 = 0;
	Tube_Num_2 = 0;
	Tube_Num_3 = 0;
	Tube_Num_4 = 0;
	/************* A1-A4 Init *************/
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; // A1-A4 Port

	// A1 Init (PB12)
	GPIOB->CRH &= ~(GPIO_CRH_CNF12_1 | GPIO_CRH_CNF12_0); // Push-pull out
	GPIOB->CRH |= (GPIO_CRH_MODE12_1 | GPIO_CRH_MODE12_0); // Speed 50MHz
	GPIOB->BSRR |= GPIO_BSRR_BR12; // A1 Off

	// A2 Init (PB13)
	GPIOB->CRH &= ~(GPIO_CRH_CNF13_1 | GPIO_CRH_CNF13_0); // Push-pull out
	GPIOB->CRH |= (GPIO_CRH_MODE13_1 | GPIO_CRH_MODE13_0); // Speed 50MHz
	GPIOB->BSRR |= GPIO_BSRR_BR13; // A2 Off

	// A3 Init (PB14)
	GPIOB->CRH &= ~(GPIO_CRH_CNF14_1 | GPIO_CRH_CNF14_0); // Push-pull out
	GPIOB->CRH |= (GPIO_CRH_MODE14_1 | GPIO_CRH_MODE14_0); // Speed 50MHz
	GPIOB->BSRR |= GPIO_BSRR_BR14; // A3 Off

	// A4 Init (PB15)
	GPIOB->CRH &= ~(GPIO_CRH_CNF15_1 | GPIO_CRH_CNF15_0); // Push-pull out
	GPIOB->CRH |= (GPIO_CRH_MODE15_1 | GPIO_CRH_MODE15_0); // Speed 50MHz
	GPIOB->BSRR |= GPIO_BSRR_BR15; // A4 Off

	/*************** K0-K9 Init **************/
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN; // Kat Port EN

	// K0 Init (PA0)
	GPIOA->CRL &= ~(GPIO_CRL_CNF0_0 | GPIO_CRL_CNF0_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE0_1 | GPIO_CRL_MODE0_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR0; // K0 Off

	// K1 Init (PA1)
	GPIOA->CRL &= ~(GPIO_CRL_CNF1_0 | GPIO_CRL_CNF1_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE1_1 | GPIO_CRL_MODE1_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR1; // K1 Off

	// K2 Init (PA2)
	GPIOA->CRL &= ~(GPIO_CRL_CNF2_0 | GPIO_CRL_CNF2_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE2_1 | GPIO_CRL_MODE2_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR2; // K2 Off

	// K3 Init (PA3)
	GPIOA->CRL &= ~(GPIO_CRL_CNF3_0 | GPIO_CRL_CNF3_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE3_1 | GPIO_CRL_MODE3_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR3; // K3 Off

	// K4 Init (PA4)
	GPIOA->CRL &= ~(GPIO_CRL_CNF4_0 | GPIO_CRL_CNF4_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE4_1 | GPIO_CRL_MODE4_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR4; // K4 Off

	// K5 Init (PA5)
	GPIOA->CRL &= ~(GPIO_CRL_CNF5_0 | GPIO_CRL_CNF5_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE5_1 | GPIO_CRL_MODE5_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR5; // K5 Off

	// K6 Init (PA6)
	GPIOA->CRL &= ~(GPIO_CRL_CNF6_0 | GPIO_CRL_CNF6_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE6_1 | GPIO_CRL_MODE6_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR6; // K6 Off

	// K7 Init (PA7)
	GPIOA->CRL &= ~(GPIO_CRL_CNF7_0 | GPIO_CRL_CNF7_1); // Push-pull out
	GPIOA->CRL |= (GPIO_CRL_MODE7_1 | GPIO_CRL_MODE7_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR7; // K7 Off

	// K8 Init (PA8)
	GPIOA->CRH &= ~(GPIO_CRH_CNF8_0 | GPIO_CRH_CNF8_1); // Push-pull out
	GPIOA->CRH |= (GPIO_CRH_MODE8_1 | GPIO_CRH_MODE8_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR8; // K8 Off

	// K9 Init (PA9)
	GPIOA->CRH &= ~(GPIO_CRH_CNF9_0 | GPIO_CRH_CNF9_1); // Push-pull out
	GPIOA->CRH |= (GPIO_CRH_MODE9_1 | GPIO_CRH_MODE9_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR9; // K9 Off

	// K, Init (PA10
	GPIOA->CRH &= ~(GPIO_CRH_CNF10_0 | GPIO_CRH_CNF10_1); // Push-pull out
	GPIOA->CRH |= (GPIO_CRH_MODE10_1 | GPIO_CRH_MODE10_0); // Speed 50MHz
	GPIOA->BSRR |= GPIO_BSRR_BR10; // K10 Off
}

inline void Tube_Set_Num_1(uint8_t num_in)
{
	Anode_All_Off();
	Katode_All_Off();

	A1_ON();
	switch (num_in) {
	case 0:
		K0_ON();
		break;
	case 1:
		K1_ON();
		break;
	case 2:
		K2_ON();
		break;
	case 3:
		K3_ON();
		break;
	case 4:
		K4_ON();
		break;
	case 5:
		K5_ON();
		break;
	case 6:
		K6_ON();
		break;
	case 7:
		K7_ON();
		break;
	case 8:
		K8_ON();
		break;
	case 9:
		K9_ON();
		break;
	default:
		Katode_All_Off();
		break;
	}
}

inline void Tube_Set_Num_2(uint8_t num_in)
{
	Anode_All_Off();
	Katode_All_Off();

	A2_ON();
	switch (num_in) {
	case 0:
		K0_ON();
		break;
	case 1:
		K1_ON();
		break;
	case 2:
		K2_ON();
		break;
	case 3:
		K3_ON();
		break;
	case 4:
		K4_ON();
		break;
	case 5:
		K5_ON();
		break;
	case 6:
		K6_ON();
		break;
	case 7:
		K7_ON();
		break;
	case 8:
		K8_ON();
		break;
	case 9:
		K9_ON();
		break;
	default:
		Katode_All_Off();
		break;
	}
}

inline void Tube_Set_Num_3(uint8_t num_in)
{
	Anode_All_Off();
	Katode_All_Off();

	A3_ON();
	switch (num_in) {
	case 0:
		K0_ON();
		break;
	case 1:
		K1_ON();
		break;
	case 2:
		K2_ON();
		break;
	case 3:
		K3_ON();
		break;
	case 4:
		K4_ON();
		break;
	case 5:
		K5_ON();
		break;
	case 6:
		K6_ON();
		break;
	case 7:
		K7_ON();
		break;
	case 8:
		K8_ON();
		break;
	case 9:
		K9_ON();
		break;
	default:
		Katode_All_Off();
		break;
	}
}

inline void Tube_Set_Num_4(uint8_t num_in)
{
	Anode_All_Off();
	Katode_All_Off();

	A4_ON();
	switch (num_in) {
	case 0:
		K0_ON();
		break;
	case 1:
		K1_ON();
		break;
	case 2:
		K2_ON();
		break;
	case 3:
		K3_ON();
		break;
	case 4:
		K4_ON();
		break;
	case 5:
		K5_ON();
		break;
	case 6:
		K6_ON();
		break;
	case 7:
		K7_ON();
		break;
	case 8:
		K8_ON();
		break;
	case 9:
		K9_ON();
		break;
	default:
		Katode_All_Off();
		break;
	}
}

inline void Tube_Conv(uint16_t num_in)
{
	Tube_Num_1 = num_in % 10;
	num_in /= 10;

	Tube_Num_2 = num_in % 10;
	num_in /= 10;

	Tube_Num_3 = num_in % 10;
	num_in /= 10;

	Tube_Num_4 = num_in % 10;
}

inline void Tube_Conv_Time(uint8_t minutes, uint8_t seconds)
{
	Tube_Num_2 = minutes % 10;
	minutes /= 10;

	Tube_Num_1 = minutes % 10;

	Tube_Num_4 = seconds % 10;
	seconds /= 10;

	Tube_Num_3 = seconds % 10;
}
