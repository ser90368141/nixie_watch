/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */
extern osSemaphoreId DS3231_TimeUpdate_SemHandle;

extern FlagTimeSet flag;
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */
void GPIO_LED_STOCK_Init(void) // Configure LED_STOCK
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

	GPIOC->CRH &= ~(GPIO_CRH_CNF13_1 | GPIO_CRH_CNF13_0); // Push-pull out
	GPIOC->CRH |= (GPIO_CRH_MODE13_1 | GPIO_CRH_MODE13_0); // Speed 50MHz

	GPIOC->BSRR |= GPIO_BSRR_BS13; // LED off
}

void GPIO_LED_STOCK_Tog()
{
	if (READ_BIT(GPIOC->IDR, GPIO_IDR_IDR13))
	{
		LED_STOCK_ON();
	}
	else
	{
		LED_STOCK_OFF();
	}
}

void But_Init(void)
{
	// But1 - PB1
	// But2 - PB3
	// But3 - PB4
	// But4 - PB5
	/*** But1 Init Start ***/
	// But1 GPIO Init
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; // Enable PORTB
	GPIOB->CRL &= ~GPIO_CRL_MODE1;
	GPIOB->CRL &= ~GPIO_CRL_CNF1_0;
	GPIOB->CRL |= GPIO_CRL_CNF1_1; // Input Pu or Pd
	GPIOB->BSRR |= GPIO_BSRR_BS1; // Pull-Up
	// But1 EXTI Init
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; // Enable AFIO
	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI1_PB; // Enable EXTI PB1
	EXTI->IMR |= EXTI_IMR_IM1; // Interrupt Enable
	EXTI->FTSR |= EXTI_FTSR_FT1; // Falling Trigger
	// Enable NVIC for EXTI1 Interrupt
	NVIC_SetPriority(EXTI1_IRQn, 6);
	NVIC_EnableIRQ(EXTI1_IRQn);
	/*** But1 Init End ***/

	/*** But2 Init Start ***/
	// But2 GPIO Init
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; // Enable PORTB
	GPIOB->CRL &= ~GPIO_CRL_MODE3;
	GPIOB->CRL &= ~GPIO_CRL_CNF3_0;
	GPIOB->CRL |= GPIO_CRL_CNF3_1; // Input Pu or Pd
	GPIOB->BSRR |= GPIO_BSRR_BS3; // Pull-Up
	// But2 EXTI Init
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; // Enable AFIO
	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI3_PB; // Enable EXTI PB3
	EXTI->IMR |= EXTI_IMR_IM3; // Interrupt Enable
	EXTI->FTSR |= EXTI_FTSR_FT3; // Falling Trigger
	// Enable NVIC for EXTI1 Interrupt
	NVIC_SetPriority(EXTI3_IRQn, 6);
	NVIC_EnableIRQ(EXTI3_IRQn);
	/*** But2 Init End ***/

	/*** But3 Init Start ***/
	// But3 GPIO Init
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; // Enable PORTB
	GPIOB->CRL &= ~GPIO_CRL_MODE4;
	GPIOB->CRL &= ~GPIO_CRL_CNF4_0;
	GPIOB->CRL |= GPIO_CRL_CNF4_1; // Input Pu or Pd
	GPIOB->BSRR |= GPIO_BSRR_BS4; // Pull-Up
	// But3 EXTI Init
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; // Enable AFIO
	AFIO->EXTICR[1] |= AFIO_EXTICR2_EXTI4_PB; // Enable EXTI PB3
	EXTI->IMR |= EXTI_IMR_IM4; // Interrupt Enable
	EXTI->FTSR |= EXTI_FTSR_FT4; // Falling Trigger
	// Enable NVIC for EXTI1 Interrupt
	NVIC_SetPriority(EXTI4_IRQn, 6);
	NVIC_EnableIRQ(EXTI4_IRQn);
	/*** But3 Init End ***/
}

inline void But1_CallBack(void)
{
	DWT_Delay_us(25000);
	if ((GPIOB->IDR & GPIO_IDR_IDR1) == 1) return;
	while ((GPIOB->IDR & GPIO_IDR_IDR1) == 1);

	DS3231_SetTime(&DS3231_DateTime);

	if (flag >= 5) flag = 0;
	else
	{
		flag++;
	}
	osSemaphoreRelease(DS3231_TimeUpdate_SemHandle); // For fast reaction task LCD

}

inline void But2_CallBack(void)
{
	DWT_Delay_us(25000);
	if ((GPIOB->IDR & GPIO_IDR_IDR1) == 1) return;
	while ((GPIOB->IDR & GPIO_IDR_IDR3) == 1);

	switch (flag) {
	case NoSet:

		break;
	case SetHour:
		if (DS3231_DateTime.Hour >= 23) DS3231_DateTime.Hour = 23;
		else DS3231_DateTime.Hour++;
		break;
	case SetMin:
		if (DS3231_DateTime.Minutes >= 59) DS3231_DateTime.Minutes = 59;
		else DS3231_DateTime.Minutes++;
		break;
	case SetYear:
		if (DS3231_DateTime.Year >= 99) DS3231_DateTime.Minutes = 99;
		else DS3231_DateTime.Year++;
		break;
	case SetMonth:
		if (DS3231_DateTime.Month >= 12) DS3231_DateTime.Month = 12;
		else DS3231_DateTime.Month++;
		break;
	case SetDate:
		if (DS3231_DateTime.Date >= 31) DS3231_DateTime.Date = 31;
		else DS3231_DateTime.Date++;
		break;
	default:
		break;
	}
}

inline void But3_CallBack(void)
{
	DWT_Delay_us(25000);
	if ((GPIOB->IDR & GPIO_IDR_IDR1) == 1) return;
	while ((GPIOB->IDR & GPIO_IDR_IDR4) == 1);

	switch (flag) {
	case NoSet:

		break;
	case SetHour:
		if (DS3231_DateTime.Hour <= 0) DS3231_DateTime.Hour = 0;
		else DS3231_DateTime.Hour--;
		break;
	case SetMin:
		if (DS3231_DateTime.Minutes <= 0) DS3231_DateTime.Minutes = 0;
		else DS3231_DateTime.Minutes--;
		break;
	case SetYear:
		if (DS3231_DateTime.Year <= 0) DS3231_DateTime.Year = 0;
		else DS3231_DateTime.Year--;
		break;
	case SetMonth:
		if (DS3231_DateTime.Month <= 1) DS3231_DateTime.Month = 1;
		else DS3231_DateTime.Month--;
		break;
	case SetDate:
		if (DS3231_DateTime.Date <= 1) DS3231_DateTime.Date = 1;
		else DS3231_DateTime.Date--;
		break;
	default:
		break;
	}
}



/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
