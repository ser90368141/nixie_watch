/*
 * LCD_I2C.c
 *
 *  Created on: 26 апр. 2018 г.
 *      Author: sergey
 */

#include "LCD_I2C.h"

uint8_t buf[1] = {0};

uint8_t LCD_WriteByteI2C(uint8_t byte)
{
	buf[0] = byte;
	uint8_t res = 0;

	taskENTER_CRITICAL();
	res = I2C_Soft_WriteBuffer(&I2C_SoftLCD, 0x4E, buf, 1);
	taskEXIT_CRITICAL();

	return res;
}
uint8_t LCD_Send_HalfByte(uint8_t c)
{
	c <<= 4;
	if (!e_set()) return 0;
	DWT_Delay_us(50);
	if (!LCD_WriteByteI2C(portlcd | c)) return 0;
	if (!e_reset()) return 0;
	DWT_Delay_us(50);
	return 1;
}
uint8_t LCD_Send_Byte(uint8_t c, uint8_t mode)
{
	if (mode == 0)
	{
		if (!rs_reset()) return 0;
	}
	else
	{
		if (!rs_set()) return 0;
	}
	uint8_t hc = 0;
	hc = c >> 4;
	if (!LCD_Send_HalfByte(hc)) return 0;
	if (!LCD_Send_HalfByte(c)) return 0;
	return 1;
}
uint8_t LCD_Clear(void)
{
	if (!LCD_Send_Byte(0x01, 0)) return 0;
	DWT_Delay_us(2000);
	return 1;
}
uint8_t LCD_Send_Char(char ch)
{
	if (!LCD_Send_Byte(ch, 1)) return 0;
	return 1;
}
uint8_t LCD_SetPos(uint8_t x, uint8_t y)
{
	switch(y)
	{
	case 0:
		if (!LCD_Send_Byte(x|0x80, 0)) return 0;
		DWT_Delay_us(1000);
		break;
	case 1:
		if (!LCD_Send_Byte((0x40 + x) | 0x80, 0)) return 0;
		DWT_Delay_us(1000);
		break;
	default:
		if (!LCD_Send_Byte(x|0x80, 0)) return 0;
		DWT_Delay_us(1000);
		break;
	}
	return 1;
}
void LCD_I2C_Init(void)
{
	DWT_Init();
	I2C_SoftLCD.Port_Periph = LL_APB2_GRP1_PERIPH_GPIOB;
	I2C_SoftLCD.SCL_GPIO_PIN = LL_GPIO_PIN_10;
	I2C_SoftLCD.SCL_GPIO_PORT = GPIOB;
	I2C_SoftLCD.SDA_GPIO_PIN = LL_GPIO_PIN_11;
	I2C_SoftLCD.SDA_GPIO_PORT = GPIOB;
	I2C_SoftLCD.I2C_Delay_us = 2;
	I2C_Soft_Init(&I2C_SoftLCD);

	portlcd = 0x00;
	DWT_Delay_us(15000);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(4000);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(100000);
	LCD_Send_HalfByte(0x03);
	DWT_Delay_us(1000);

	LCD_Send_HalfByte(0x02);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x28, 0);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x0C, 0);
	DWT_Delay_us(1000);

	LCD_Send_Byte(0x01, 0);
	DWT_Delay_us(2000);

	LCD_Send_Byte(0x06, 0);
	DWT_Delay_us(1000);

	led_set();
	write_set();
}

uint8_t LCD_Send_Str(char* str)
{
	uint8_t i = 0;
	while(str[i] != 0)
	{
		if (!LCD_Send_Byte(str[i], 1)) return 0;
		i++;
	}
	return  1;
}


void LCD_OutTime(char *str, DS3231_DateTime_struct *time_struct)
{
	sprintf(str, "Time ");
	itoa((time_struct->Hour / 10) % 10, &str[5], 10);
	itoa(time_struct->Hour % 10, &str[6], 10);
	str[7] = ':';
	itoa((time_struct->Minutes / 10) % 10, &str[8], 10);
	itoa(time_struct->Minutes % 10, &str[9], 10);
	str[10] = ':';
	itoa((time_struct->Seconds / 10) % 10, &str[11], 10);
	itoa(time_struct->Seconds % 10, &str[12], 10);
	str[13] = ' ';
	str[14] = ' ';
	str[15] = ' ';
}

void LCD_OutDate(char *str, DS3231_DateTime_struct *time_struct)
{
	sprintf(str, "Date ");
	itoa((time_struct->Date / 10) % 10, &str[5], 10);
	itoa(time_struct->Date % 10, &str[6], 10);
	str[7] = '.';
	itoa((time_struct->Month / 10) % 10, &str[8], 10);
	itoa(time_struct->Month % 10, &str[9], 10);
	str[10] = '.';
	str[11] = '2';
	str[12] = '0';
	itoa((time_struct->Year / 10) % 10, &str[13], 10);
	itoa(time_struct->Year % 10, &str[14], 10);
	str[15] = ' ';
}
