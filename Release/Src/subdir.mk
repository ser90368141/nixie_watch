################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/LCD_I2C.c \
../Src/Tube_Ind.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/i2c.c \
../Src/main.c \
../Src/stm32f1xx_hal_msp.c \
../Src/stm32f1xx_hal_timebase_TIM.c \
../Src/stm32f1xx_it.c \
../Src/system_stm32f1xx.c 

OBJS += \
./Src/LCD_I2C.o \
./Src/Tube_Ind.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/i2c.o \
./Src/main.o \
./Src/stm32f1xx_hal_msp.o \
./Src/stm32f1xx_hal_timebase_TIM.o \
./Src/stm32f1xx_it.o \
./Src/system_stm32f1xx.o 

C_DEPS += \
./Src/LCD_I2C.d \
./Src/Tube_Ind.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/i2c.d \
./Src/main.d \
./Src/stm32f1xx_hal_msp.d \
./Src/stm32f1xx_hal_timebase_TIM.d \
./Src/stm32f1xx_it.d \
./Src/system_stm32f1xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Inc" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Drivers/CMSIS/Include" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Inc" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM3" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Middlewares/Third_Party/FreeRTOS/Source/include" -I"/media/sergey/C28A03F38A03E2B1/Users/Sergey/Documents/MEGA/STM/STM32_Project/Nixie Tube Watch/STM32_Project/F103_Nixie_HAL_1/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


