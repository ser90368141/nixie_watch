/*
 * DS3231.c
 *
 *  Created on: 28 апр. 2018 г.
 *      Author: sergey
 */

#include "DS3231.h"

void I2C_WriteBuffer(uint8_t sizebuf)
{
	taskENTER_CRITICAL();
	I2C_Soft_WriteBuffer(&I2C_SoftDS, DS3231_ADDR, (uint8_t*) &DS3231_Buf, (uint32_t)sizebuf);
	taskEXIT_CRITICAL();
}

void I2C_ReadBuffer(uint8_t sizebuf)
{
	taskENTER_CRITICAL();
	I2C_Soft_ReadBuffer(&I2C_SoftDS, DS3231_ADDR, (uint8_t*) &DS3231_Buf, (uint32_t)sizebuf);
	taskEXIT_CRITICAL();
}

void DS3231_Init(void)
{
	/* Initialize I2C */
	//	MX_I2C1_Init();
	DWT_Init();
	I2C_SoftDS.Port_Periph = LL_APB2_GRP1_PERIPH_GPIOB;
	I2C_SoftDS.SCL_GPIO_PIN = LL_GPIO_PIN_6;
	I2C_SoftDS.SCL_GPIO_PORT = GPIOB;
	I2C_SoftDS.SDA_GPIO_PIN = LL_GPIO_PIN_7;
	I2C_SoftDS.SDA_GPIO_PORT = GPIOB;
	I2C_SoftDS.I2C_Delay_us = 2;
	I2C_Soft_Init(&I2C_SoftDS);

	DS3231_Start_1Hz();

	DS3231_SQW_Init();
}

void DS3231_GetTime(DS3231_DateTime_struct *time_struct)
{

	DS3231_Buf[0] = 0;
	I2C_WriteBuffer(1);

	I2C_ReadBuffer(7);

	time_struct->Date = RTC_ConvertFromDec(DS3231_Buf[4]);
	time_struct->Month = RTC_ConvertFromDec(DS3231_Buf[5]);
	time_struct->Year = RTC_ConvertFromDec(DS3231_Buf[6]);
	time_struct->Day = RTC_ConvertFromDec(DS3231_Buf[3]);
	time_struct->Hour = RTC_ConvertFromDec(DS3231_Buf[2]);
	time_struct->Minutes = RTC_ConvertFromDec(DS3231_Buf[1]);
	time_struct->Seconds = RTC_ConvertFromDec(DS3231_Buf[0]);
}

void DS3231_SetTime(DS3231_DateTime_struct *time_struct)
{

	DS3231_Buf[0] = 0; // Start write in 0 reg
	/* Convert time to raw data */
	DS3231_Buf[1] = RTC_ConvertFromBinDec(time_struct->Seconds);
	DS3231_Buf[2] = RTC_ConvertFromBinDec(time_struct->Minutes);
	DS3231_Buf[3] = RTC_ConvertFromBinDec(time_struct->Hour);
	DS3231_Buf[4] = RTC_ConvertFromBinDec(time_struct->Day);
	DS3231_Buf[5] = RTC_ConvertFromBinDec(time_struct->Date);
	DS3231_Buf[6] = RTC_ConvertFromBinDec(time_struct->Month);
	DS3231_Buf[7] = RTC_ConvertFromBinDec(time_struct->Year);

	I2C_WriteBuffer(8);
}

void DS3231_Start_1Hz(void)
{
	DS3231_Buf[0] = 0x0E; // Control Reg
	DS3231_Buf[1] = 0b00100000; // 1Hz ON

	I2C_WriteBuffer(2);
}

void DS3231_SQW_Init(void)
{
	// SQW <-> PB8
	// Init PB8
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; // Enable PORT B
	GPIOB->CRH &= ~GPIO_CRH_CNF8_0;
	GPIOB->CRH |= GPIO_CRH_CNF8_1;
	GPIOB->CRH &= ~GPIO_CRH_MODE8; // Input
	GPIOB->BSRR |= GPIO_BSRR_BS8; // Pull-Up
	// Init EXTI PB8
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; // Enable Alt Func
	AFIO->EXTICR[2] |= AFIO_EXTICR3_EXTI8_PB; // PB8 EXTI Enable
	EXTI->IMR |= EXTI_IMR_IM8; // Interrapt Enable
	EXTI->FTSR |= EXTI_FTSR_FT8; // Falling Trigger

	NVIC_SetPriority(EXTI9_5_IRQn, 5);
	NVIC_EnableIRQ(EXTI9_5_IRQn); // Enable IRQ Exti
	__enable_irq(); // Enable IRQ
}

uint8_t RTC_ConvertFromDec(uint8_t c)
{
	uint8_t ch = ((c>>4)*10+(0x0F&c));
	return ch;
}

uint8_t RTC_ConvertFromBinDec(uint8_t c)
{
	uint8_t ch = ((c/10)<<4)|(c%10);
	return ch;
}
