/*
 * DS3231.h
 *
 *  Created on: 28 апр. 2018 г.
 *      Author: sergey
 */

#ifndef DS3231_H_
#define DS3231_H_

//#include "i2c.h"
#include "I2C_Soft_LL.h"

#define DS3231_ADDR 0xD0

typedef struct DS3231_DateTime_struct_type{
	char Seconds;
	char Minutes;
	char Hour;
	char Day;// Range 1-7
	char Date;// Range 1-31
	char Month;
	char Year;// Range 0-99
} DS3231_DateTime_struct;

//---------------
I2C_Soft_TypeDef I2C_SoftDS;
//---------------

DS3231_DateTime_struct DS3231_DateTime;

uint8_t DS3231_Buf[8];

void I2C_WriteBuffer(uint8_t sizebuf);
void I2C_ReadBuffer(uint8_t sizebuf);

void DS3231_Init(void);
void DS3231_GetTime(DS3231_DateTime_struct *time_str);
void DS3231_SetTime(DS3231_DateTime_struct *time_str);
void DS3231_Start_1Hz(void);
void DS3231_SQW_Init(void);

uint8_t RTC_ConvertFromDec(uint8_t c);
uint8_t RTC_ConvertFromBinDec(uint8_t c);

#endif /* DS3231_H_ */
