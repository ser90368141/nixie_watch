/*
 * DWT_Delay.h
 *
 *  Created on: 23 сент. 2018 г.
 *      Author: sergey
 */

#ifndef DWT_DELAY_DWT_DELAY_H_
#define DWT_DELAY_DWT_DELAY_H_

#include "stm32f1xx.h"

#define    DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
#define    DWT_CONTROL   *(volatile unsigned long *)0xE0001000
#define    SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC

static  uint8_t flag_DWT_Init = 0;

static inline void DWT_Delay_us( uint32_t us)
{
	uint32_t t0 = DWT->CYCCNT;
	uint32_t us_count_tic = (us * (SystemCoreClock/1000000)) - 10;
	while ((DWT->CYCCNT - t0) < us_count_tic) ;
}

static inline void DWT_Init(void)
{
	if (!flag_DWT_Init)
	{
		//разрешаем использовать счётчик
		SCB_DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
		//обнуляем значение счётного регистра
		DWT_CYCCNT  = 0;
		//запускаем счётчик
		DWT_CONTROL |= DWT_CTRL_CYCCNTENA_Msk;
		flag_DWT_Init = 1;
	}
}

#endif /* DWT_DELAY_DWT_DELAY_H_ */
