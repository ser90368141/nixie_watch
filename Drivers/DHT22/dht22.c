/*
 * dht22.c
 *
 *  Created on: 17 ����. 2018 �.
 *      Author: Sergey
 */

#include "dht22.h"
//--------------------------------------------------
void port_init(void)
{
	// Port configuration A12
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN; // RCC Port A
	DHT_PORT->CRH |= GPIO_CRH_MODE12; // Speed 50MHz
	DHT_PORT->CRH |= GPIO_CRH_CNF12_0;
	DHT_PORT->CRH &= ~GPIO_CRH_CNF12_1;
}
//--------------------------------------------------
uint8_t dht22_init()
{
	port_init();

	DWT_Init();

	//DWT_Delay_us(1000000);
	DHT_PORT->ODR |= GPIO_ODR_ODR12;// High State

	temp = 0;
	hum = 0;
	for (uint8_t t = 0; t < 5; t++)
	{
		dt_raw[t] = 0;
	}

	i = 0;
	j = 0;

	return 0;
}
//--------------------------------------------------
uint8_t dht22_GetDataRaw(uint8_t *data)
{
	uint32_t t = 0;
	i = 0;
	j = 0;
	//reset port
	DHT_PORT->ODR &= ~GPIO_ODR_ODR12;
	DHT_PORT->ODR |= GPIO_ODR_ODR12;
	DWT_Delay_us(100000);
	//передадим условие СТАРТ
	DHT_PORT->ODR &= ~GPIO_ODR_ODR12;
	DWT_Delay_us(18000);
	DHT_PORT->ODR |= GPIO_ODR_ODR12;
	//дождемся ответа датчика
	DWT_Delay_us(39);//20-40 us
	if(DHT_PORT->IDR & GPIO_IDR_IDR12)
	{
		return 0;
	}
	DWT_Delay_us(100);
	//если датчик не отпустил шину, то ошибка
	if(!(DHT_PORT->IDR & GPIO_IDR_IDR12))
	{
		return 0;
	}
	DWT_Delay_us(80);
	//читаем данные (записываем байты в массив наоборот,
	//так как сначала передаётся старший, чтобы потом
	//не переворачивать двухбайтовый результат)
	for (j = 0; j < 5; j++)
	{
		data[4 - j] = 0;
		for(i = 0; i < 8; i++)
		{
			t = 0;
			while(!(DHT_PORT->IDR & GPIO_IDR_IDR12)) //ждём отпускания шины
			{
				t++;
				if (t >= 100000) return 0;
			}
			DWT_Delay_us(40);
			if(DHT_PORT->IDR & GPIO_IDR_IDR12) //читаем результат по прошествии 30 микросекунд
			{
				data[4-j] |= (1<<(7-i));
			}
			t = 0;
			while(DHT_PORT->IDR & GPIO_IDR_IDR12) //ждём, пока датчик притянет шину (в случае единицы)
			{
				t++;
				if (t >= 100000) return 0;
			}
		}
	}
	return 1;
}
//--------------------------------------------------
uint8_t dht22_GetData()
{
	if (dht22_GetDataRaw(dt_raw))
	{
		return 1;
	}
	else return 0;

}
//--------------------------------------------------
inline void dht22_GetDataWithRTOS_1()
{
	i = 0;
	j = 0;
	//reset port
	DHT_PORT->ODR &= ~GPIO_ODR_ODR12;
	DHT_PORT->ODR |= GPIO_ODR_ODR12;
	// Use RTOS Delay 100000us
}
//--------------------------------------------------
inline void dht22_GetDataWithRTOS_2()
{
	//передадим условие СТАРТ
	DHT_PORT->ODR &= ~GPIO_ODR_ODR12;
	// Use RTOS Delay 18000us
}
//--------------------------------------------------
uint8_t dht22_GetDataWithRTOS_3()
{
	uint32_t t = 0;
	DHT_PORT->ODR |= GPIO_ODR_ODR12;
	//дождемся ответа датчика
	DWT_Delay_us(39);//20-40 us
	if(DHT_PORT->IDR & GPIO_IDR_IDR12)
	{
		return 0;
	}
	DWT_Delay_us(100);
	//если датчик не отпустил шину, то ошибка
	if(!(DHT_PORT->IDR & GPIO_IDR_IDR12))
	{
		return 0;
	}
	DWT_Delay_us(80);
	//читаем данные (записываем байты в массив наоборот,
	//так как сначала передаётся старший, чтобы потом
	//не переворачивать двухбайтовый результат)
	for (j = 0; j < 5; j++)
	{
		dt_raw[4 - j] = 0;
		for(i = 0; i < 8; i++)
		{
			t = 0;
			while(!(DHT_PORT->IDR & GPIO_IDR_IDR12)) //ждём отпускания шины
			{
				t++;
				if (t >= 100000) return 0;
			}
			DWT_Delay_us(40);
			if(DHT_PORT->IDR & GPIO_IDR_IDR12) //читаем результат по прошествии 30 микросекунд
			{
				dt_raw[4-j] |= (1<<(7-i));
			}
			t = 0;
			while(DHT_PORT->IDR & GPIO_IDR_IDR12) //ждём, пока датчик притянет шину (в случае единицы)
			{
				t++;
				if (t >= 100000) return 0;
			}
		}
	}
	return 1;
}
//--------------------------------------------------
float dht22_GetTemp(void)
{
	float temperature = 0;
	temperature = (float)((*(uint16_t*)(dt_raw+1)) & 0x3FFF) / 10;
	if((*(uint16_t*)(dt_raw+1)) & 0x8000) temperature *= -1.0;
	return temperature;
}
//--------------------------------------------------
float dht22_GetHum(void)
{
	float hum = 0;
	hum = (float)(*(int16_t*)(dt_raw+3)) / 10;
	return hum;
}
