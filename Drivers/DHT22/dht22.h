/*
 * dht22.h
 *
 *  Created on: 17 ����. 2018 �.
 *      Author: Sergey
 */

#ifndef DHT22_H_
#define DHT22_H_

//--------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "DWT_Delay.h"
//--------------------------------------------------
#define DHT_PORT GPIOA
//--------------------------------------------------
uint8_t dt_raw[5]; //// Внутренний массив сырых данных, заполняется при вызове dht22_GetData
uint8_t i, j;
float temp;
float hum;
//--------------------------------------------------
void port_init(void); // Функция инициализации порта (если не используется Cube)
uint8_t dht22_init(void); // Инициализация датичка
uint8_t dht22_GetDataRaw(uint8_t *data); // Низкоуровневая функция, данные в data в сыром виде
uint8_t dht22_GetData(void); // Функция обращения к датчику, запускать с интервалом в 2 сек.
float dht22_GetTemp(void); // Вывод температуры, данные берутся из dt_raw
float dht22_GetHum(void); // Вывод влажности, данные берутся из dt_raw
//--------------------------------------------------
void dht22_GetDataWithRTOS_1();
void dht22_GetDataWithRTOS_2();
uint8_t dht22_GetDataWithRTOS_3();
#endif /* DHT22_H_ */
