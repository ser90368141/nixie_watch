/*
 * Tube_Ind.h
 *
 *  Created on: 21 апр. 2018 г.
 *      Author: sergey
 */

#ifndef TUBE_IND_H_
#define TUBE_IND_H_

#include "gpio.h"

/***************** Anode DEFINE *****************/
#define Anode_All_Off() GPIOB->BSRR |= (GPIO_BSRR_BR12|GPIO_BSRR_BR13|GPIO_BSRR_BR14|GPIO_BSRR_BR15)

#define A1_ON() GPIOB->BSRR |= GPIO_BSRR_BS12
#define A1_OFF() GPIOB->BSRR |= GPIO_BSRR_BR12

#define A2_ON() GPIOB->BSRR |= GPIO_BSRR_BS13
#define A2_OFF() GPIOB->BSRR |= GPIO_BSRR_BR13

#define A3_ON() GPIOB->BSRR |= GPIO_BSRR_BS14
#define A3_OFF() GPIOB->BSRR |= GPIO_BSRR_BR14

#define A4_ON() GPIOB->BSRR |= GPIO_BSRR_BS15
#define A4_OFF() GPIOB->BSRR |= GPIO_BSRR_BR15

/***************** Katode DEFINE *****************/
#define Katode_All_Off() GPIOA->BSRR |= (GPIO_BSRR_BR0 | GPIO_BSRR_BR1 | GPIO_BSRR_BR2 | GPIO_BSRR_BR3 \
		| GPIO_BSRR_BR4 | GPIO_BSRR_BR5 | GPIO_BSRR_BR6 | GPIO_BSRR_BR7 | GPIO_BSRR_BR8 | GPIO_BSRR_BR8 \
		| GPIO_BSRR_BR9 | GPIO_BSRR_BR10)

#define K0_ON() GPIOA->BSRR |= GPIO_BSRR_BS0
#define K0_OFF() GPIOA->BSRR |= GPIO_BSRR_BR0

#define K1_ON() GPIOA->BSRR |= GPIO_BSRR_BS1
#define K1_OFF() GPIOA->BSRR |= GPIO_BSRR_BR1

#define K2_ON() GPIOA->BSRR |= GPIO_BSRR_BS2
#define K2_OFF() GPIOA->BSRR |= GPIO_BSRR_BR2

#define K3_ON() GPIOA->BSRR |= GPIO_BSRR_BS3
#define K3_OFF() GPIOA->BSRR |= GPIO_BSRR_BR3

#define K4_ON() GPIOA->BSRR |= GPIO_BSRR_BS4
#define K4_OFF() GPIOA->BSRR |= GPIO_BSRR_BR4

#define K5_ON() GPIOA->BSRR |= GPIO_BSRR_BS5
#define K5_OFF() GPIOA->BSRR |= GPIO_BSRR_BR5

#define K6_ON() GPIOA->BSRR |= GPIO_BSRR_BS6
#define K6_OFF() GPIOA->BSRR |= GPIO_BSRR_BR6

#define K7_ON() GPIOA->BSRR |= GPIO_BSRR_BS7
#define K7_OFF() GPIOA->BSRR |= GPIO_BSRR_BR7

#define K8_ON() GPIOA->BSRR |= GPIO_BSRR_BS8
#define K8_OFF() GPIOA->BSRR |= GPIO_BSRR_BR8

#define K9_ON() GPIOA->BSRR |= GPIO_BSRR_BS9
#define K9_OFF() GPIOA->BSRR |= GPIO_BSRR_BR9

#define K10_ON() GPIOA->BSRR |= GPIO_BSRR_BS10
#define K10_OFF() GPIOA->BSRR |= GPIO_BSRR_BR10

/***************** Variables **************************/
uint8_t Tube_Num_1;
uint8_t Tube_Num_2;
uint8_t Tube_Num_3;
uint8_t Tube_Num_4;

/***************** Function Prototype *****************/
void Tube_Init(void);

void Tube_Set_Num_1(uint8_t num_in);
void Tube_Set_Num_2(uint8_t num_in);
void Tube_Set_Num_3(uint8_t num_in);
void Tube_Set_Num_4(uint8_t num_in);

void Tube_Conv(uint16_t num_in);
void Tube_Conv_Time(uint8_t minutes, uint8_t seconds);

#endif /* TUBE_IND_H_ */
