/*
 * LCD_I2C.h
 *
 *  Created on: 26 апр. 2018 г.
 *      Author: sergey
 */

#ifndef LCD_I2C_H_
#define LCD_I2C_H_

//---------------
#include <stdio.h>
#include "stdlib.h"

#include "DS3231.h"
#include "DWT_Delay.h"
#include "I2C_Soft_LL.h"

#include "cmsis_os.h"
//---------------
uint8_t portlcd;
//---------------
I2C_Soft_TypeDef I2C_SoftLCD;
//---------------
uint8_t LCD_WriteByteI2C(uint8_t byte);
static inline uint8_t e_set()
{
	return LCD_WriteByteI2C(portlcd|=0x04);
}
static inline uint8_t e_reset()
{
	return LCD_WriteByteI2C(portlcd&=~0x04);
}
static inline uint8_t rs_set()
{
	return LCD_WriteByteI2C(portlcd|=0x01);
}
static inline uint8_t rs_reset()
{
	return LCD_WriteByteI2C(portlcd&=~0x01);
}
static inline uint8_t led_set()
{
	return LCD_WriteByteI2C(portlcd|=0x08);
}
static inline uint8_t led_reset()
{
	return LCD_WriteByteI2C(portlcd&=~0x08);
}
static inline uint8_t read_set()
{
	return LCD_WriteByteI2C(portlcd|=0x02);
}
static inline uint8_t write_set()
{
	return LCD_WriteByteI2C(portlcd&=~0x02);
}
//---------------
uint8_t LCD_Send_HalfByte(uint8_t);
uint8_t LCD_Send_Byte(uint8_t, uint8_t);
uint8_t LCD_Clear(void);
uint8_t LCD_Send_Char(char ch);
uint8_t LCD_SetPos(uint8_t x, uint8_t y);
void LCD_I2C_Init(void);
uint8_t LCD_Send_Str(char* str);

void LCD_OutTime(char *str, DS3231_DateTime_struct *time_struct);
void LCD_OutDate(char *str, DS3231_DateTime_struct *time_struct);

#endif /* LCD_I2C_H_ */
